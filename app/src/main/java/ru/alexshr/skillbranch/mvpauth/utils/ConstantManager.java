package ru.alexshr.skillbranch.mvpauth.utils;


public interface ConstantManager {

    String LOG_TAG = "DEV ";


    String AUTH_TOKEN_KEY = "AUTH_TOKEN_KEY";


    String PRODUCT_KEY = "PRODUCT_KEY";

    String TEST_TOKEN = "TEST_TOKEN";
    String ENTER_DATE_KEY = "ENTER_DATE_KEY";
    String TAG_LOGIN_FRAGMENT = "TAG_LOGIN_FRAGMENT";

    String MES_AUTH_FINISHED = "MES_AUTH_FINISHED";

    String BACKET_COUNT_KEY = "BACKET_COUNT_KEY";
}

