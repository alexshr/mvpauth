package ru.alexshr.skillbranch.mvpauth.mvp.models;

import java.util.List;

import ru.alexshr.skillbranch.mvpauth.data.managers.DataManager;
import ru.alexshr.skillbranch.mvpauth.data.storage.dto.ProductDTO;

public class CatalogModel {

    DataManager mDataManager = DataManager.getInstance();

    public List<ProductDTO> getProductList() {
        return mDataManager.getProductList();
    }

    public boolean isUserAuth() {
        return mDataManager.isUserAuth();
    }

    public int getBasketCount() {
        return mDataManager.getBasketCount();
    }

    public void addProductToBacket(ProductDTO product) {
        mDataManager.addProductToBasket(product);
    }
}
