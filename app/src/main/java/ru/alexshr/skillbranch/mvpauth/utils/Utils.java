package ru.alexshr.skillbranch.mvpauth.utils;


import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.regex.Pattern;

import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.LOG_TAG;

public class Utils {

    public static boolean validate(String str, Pattern pattern) {
        return str != null && !str.trim().isEmpty() &&
                pattern.matcher(str.trim()).matches();
    }

    //http://stackoverflow.com/questions/1016896/get-screen-dimensions-in-pixels
    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);

        return size.x;
    }

    /**
     * prepare picasso instance to use by Picasso.with()
     *
     * @param context
     */
    public static void initPicasso(Context context) {
        OkHttp3Downloader okHttp3Downloader = new OkHttp3Downloader(context, Integer.MAX_VALUE);
        Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(okHttp3Downloader);
        Picasso picassoInstance = builder.build();
        try {
            Picasso.setSingletonInstance(picassoInstance);
            Log.d(LOG_TAG, "initPicasso");
        } catch (IllegalStateException e) {
            Log.d(LOG_TAG, "invalid attemption to init picasso (already done before)");
            // Picasso instance was already set
            // cannot set it after Picasso.with(Context) was already in use
        }
    }
}
