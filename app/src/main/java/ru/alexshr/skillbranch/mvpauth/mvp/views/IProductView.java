package ru.alexshr.skillbranch.mvpauth.mvp.views;

import ru.alexshr.skillbranch.mvpauth.data.storage.dto.ProductDTO;

public interface IProductView extends IView {

    void showProductView(ProductDTO product);

    void updateProductCountView(ProductDTO product);
}
