package ru.alexshr.skillbranch.mvpauth.mvp.views;


public interface IView {

    void showMessage(String message);

    void showMessage(int messageResId);

    void showErrors(Throwable e);

    void showLoad();

    void hideLoad();

}
