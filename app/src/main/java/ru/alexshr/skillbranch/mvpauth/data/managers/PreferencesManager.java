package ru.alexshr.skillbranch.mvpauth.data.managers;

import android.content.SharedPreferences;

import ru.alexshr.skillbranch.mvpauth.utils.ConstantManager;

import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.BACKET_COUNT_KEY;

public class PreferencesManager {
    private SharedPreferences mSharedPreferences;

    public PreferencesManager() {
        mSharedPreferences = MvpApplication.getSharedPreferences();
    }

    public void saveAuthToken(String authToken) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(ConstantManager.AUTH_TOKEN_KEY, authToken);
        editor.apply();
    }

    public String getAuthToken() {
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN_KEY, null);
    }

    public String getEnterDate() {
        return mSharedPreferences.getString(ConstantManager.ENTER_DATE_KEY, null);
    }

    public boolean checkAuthToken() {
        return getAuthToken() != null;
    }

    public int getBacketCount() {
        return mSharedPreferences.getInt(BACKET_COUNT_KEY, 0);
    }

    public void setBacketCount(int count) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(BACKET_COUNT_KEY, count);
        editor.apply();
    }
}
