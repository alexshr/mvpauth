package ru.alexshr.skillbranch.mvpauth.mvp.presenters;


import ru.alexshr.skillbranch.mvpauth.data.storage.dto.ProductDTO;
import ru.alexshr.skillbranch.mvpauth.mvp.views.IProductView;

public class ProductPresenter extends AbstrastPresenter<IProductView> {

    private ProductDTO mProductDTO;

    public static ProductPresenter newOutInstance(ProductDTO ProductDTO) {
        return new ProductPresenter(ProductDTO);
    }

    private ProductPresenter(ProductDTO product) {
        mProductDTO = product;
    }


    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProductDTO);
        }
    }

    public void clickOnPlus() {
        mProductDTO.incrementCount();
        if (getView() != null) {
            getView().updateProductCountView(mProductDTO);
        }
    }


    public void clickOnMinus() {
        if (mProductDTO.getCount() > 0) {
            mProductDTO.decrementCount();
            if (getView() != null) {
                getView().updateProductCountView(mProductDTO);
            }
        }
    }
}
