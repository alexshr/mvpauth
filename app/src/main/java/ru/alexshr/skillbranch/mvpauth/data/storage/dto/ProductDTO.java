package ru.alexshr.skillbranch.mvpauth.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

public class ProductDTO implements Parcelable {

    private int id;
    private String productName;
    private String imageUrl;
    private String description;
    private int price;
    private int count;

    public ProductDTO(int id, String productName, String imageUrl, String description, int price, int count) {
        this.id = id;
        this.productName = productName;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
        this.count = count;
    }


    public void decrementCount() {
        if (count > 1) {
            count--;
        }
    }

    public void incrementCount() {
        count++;
    }


    public int getId() {
        return id;
    }

    public String getProductName() {
        return productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public int getPrice() {
        return price;
    }

    public int getCount() {
        return count;
    }

    public String getDescription() {
        return description;
    }

//region parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.productName);
        dest.writeString(this.imageUrl);
        dest.writeString(this.description);
        dest.writeInt(this.price);
        dest.writeInt(this.count);
    }

    protected ProductDTO(Parcel in) {
        this.id = in.readInt();
        this.productName = in.readString();
        this.imageUrl = in.readString();
        this.description = in.readString();
        this.price = in.readInt();
        this.count = in.readInt();
    }

    public static final Parcelable.Creator<ProductDTO> CREATOR = new Parcelable.Creator<ProductDTO>() {
        @Override
        public ProductDTO createFromParcel(Parcel source) {
            return new ProductDTO(source);
        }

        @Override
        public ProductDTO[] newArray(int size) {
            return new ProductDTO[size];
        }
    };

    //endregion---------
}
