package ru.alexshr.skillbranch.mvpauth.utils;


import java.util.regex.Pattern;

public interface AppConfig {

    Pattern EMAIL_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\._%\\-]{3,256}" +
                    "@[a-zA-Z0-9]{2,64}" +
                    "\\.[a-zA-Z0-9]{2,25}"
    );

    Pattern PASSWORD_PATTERN = Pattern.compile(
            "^[a-zA-Z0-9@#$%!]{8,}$"
    );
}
