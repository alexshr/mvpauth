package ru.alexshr.skillbranch.mvpauth.data.managers;

import android.content.Context;
import android.os.Handler;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import ru.alexshr.skillbranch.mvpauth.data.events.MessageEvent;
import ru.alexshr.skillbranch.mvpauth.data.storage.dto.ProductDTO;

import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.MES_AUTH_FINISHED;
import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.TEST_TOKEN;

public class DataManager {
    private static DataManager ourInstance;
    private PreferencesManager mPreferencesManager;
    private Context mContext;

    private List<ProductDTO> mMockProductList;

    private DataManager() {
        mPreferencesManager = new PreferencesManager();
        mContext = MvpApplication.getAppContext();

        generateMockData();
    }

    public static DataManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new DataManager();
        }
        return ourInstance;
    }


    public PreferencesManager getPreferencesManager() {
        return mPreferencesManager;
    }

    public Context getAppContext() {
        return mContext;
    }


    public ProductDTO getProductById(int productId) {
        return mMockProductList.get(productId + 1);
    }

    public void updateProduct(ProductDTO productDto) {

    }

    public List<ProductDTO> getProductList() {
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();

        mMockProductList.add(new ProductDTO(1,
                "Смартфон YotaPhone 2 Black (YD201)",
                "http://img.mvideo.ru/Pdb/30021990m.jpg",
                "Смартфон Yota YOTAPHONE YD201- оригинальный девайс с двумя экранами, работающий под управлением ОС Android 4.4.",
                19990, 1));

        mMockProductList.add(new ProductDTO(2,
                "Смартфон Samsung Galaxy A3 SM-A300F White",
                "http://img.mvideo.ru/Pdb/30021724m.jpg",
                "Смартфон Samsung Galaxy A3 сочетает в себе высокую функциональность и стильный дизайн.",
                12990, 1));

        mMockProductList.add(new ProductDTO(3,
                "Смартфон Prestigio Wize D3 3505 Duo",
                "http://img.mvideo.ru/Pdb/30022743m.jpg",
                "Смартфон Prestigio PS P3505 DUO - простой в использовании аппарат, работающий под управлением популярной ОС Android 4.4.",
                3815, 1));

        mMockProductList.add(new ProductDTO(4,
                "Смартфон Huawei Honor 4С Gold (СНМ-U01)",
                "http://img.mvideo.ru/Pdb/30024346m.jpg",
                "Huawei Honor 4C - современный смартфон с широкими возможностями. Его можно использоваться и для телефонных звонков, и для доступа в интернет, и для развлечений.",
                9990, 1));

        mMockProductList.add(new ProductDTO(5,
                "Смартфон Samsung Galaxy J mini SM-J105H/DS Black",
                "http://img.mvideo.ru/Pdb/30024621m.jpg",
                "Samsung Galaxy J mini - компактный смартфон с 4-дюймовым экраном. Это хороший выбор для того, кому в первую очередь нужен надёжный аппарат для общения.",
                4990, 1));
    }

    public boolean isUserAuth() {
        return mPreferencesManager.checkAuthToken();
    }

    public void login(String email, String password) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mPreferencesManager.saveAuthToken(TEST_TOKEN);
                EventBus.getDefault().post(new MessageEvent(MES_AUTH_FINISHED));
            }
        }, 1000);
    }

    public int getBasketCount() {
        return mPreferencesManager.getBacketCount();
    }

    public void addProductToBasket(ProductDTO product) {
        int productCount = product.getCount();
        int backetCount = getBasketCount();
        mPreferencesManager.setBacketCount(backetCount + productCount);
    }
}
