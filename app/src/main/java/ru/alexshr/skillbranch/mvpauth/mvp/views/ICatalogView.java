package ru.alexshr.skillbranch.mvpauth.mvp.views;

import java.util.List;

import ru.alexshr.skillbranch.mvpauth.data.storage.dto.ProductDTO;

public interface ICatalogView extends IView {

    void showAddToCardMessage(ProductDTO productDto);

    void showCatalogView(List<ProductDTO> productList);

    void showLoginScreen();

    void dismissLoginScreen();

    void invalidateOptionsMenu();
}
