package ru.alexshr.skillbranch.mvpauth.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.alexshr.skillbranch.mvpauth.BuildConfig;
import ru.alexshr.skillbranch.mvpauth.R;
import ru.alexshr.skillbranch.mvpauth.mvp.views.IView;
import ru.alexshr.skillbranch.mvpauth.ui.fragments.AccountFragment;
import ru.alexshr.skillbranch.mvpauth.ui.fragments.CatalogFragment;
import ru.alexshr.skillbranch.mvpauth.ui.fragments.FavoriteFragment;
import ru.alexshr.skillbranch.mvpauth.ui.fragments.NotificationsFragment;
import ru.alexshr.skillbranch.mvpauth.ui.fragments.OrdersFragment;
import ru.alexshr.skillbranch.mvpauth.utils.BorderedCircleTransform;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.LOG_TAG;

public class RootActivity extends AppCompatActivity
        implements IView {

//region butterknife

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

//endregion

    private FragmentManager mFragmentManager;

    private ProgressDialog mProgressDialog;

    private MaterialDialog mExitDialog;


    //region lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        //hide status bar
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);

        setupToolbar();
        setupDrawer();

        mExitDialog = new MaterialDialog.Builder(this)
                .title(R.string.app_exit)
                .content(R.string.confirm_app_exit)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        finish();
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    }
                })
                .build();


        mFragmentManager = getSupportFragmentManager();


        if (savedInstanceState == null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            mExitDialog.show();
        }
    }

    /**
     * for Calligraphy library (custom fonts)
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

//endregion

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    private void setupDrawer() {
        mNavigationView.getMenu().getItem(1).setChecked(true);

        mNavigationView.setNavigationItemSelectedListener(item -> {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.nav_account:
                    fragment = new AccountFragment();
                    break;
                case R.id.nav_catalog:
                    fragment = new CatalogFragment();
                    break;
                case R.id.nav_favorites:
                    fragment = new FavoriteFragment();
                    break;
                case R.id.nav_orders:
                    fragment = new OrdersFragment();
                    break;
                case R.id.nav_notifications:
                    fragment = new NotificationsFragment();
                    break;
            }

            if (fragment != null) {
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, fragment)
                        .addToBackStack(null)
                        .commit();
            }

            mDrawerLayout.closeDrawer(GravityCompat.START);
            return true;
        });

        ImageView userAvatarImg = (ImageView) mNavigationView.getHeaderView(0).findViewById(R.id.avatar);
        Picasso.with(this)
                .load(R.drawable.user_avatar)
                .fit()
                //.resize(mProfileImageSize, mProfileImageSize)
                //.centerInside()
                .placeholder(R.drawable.user_avatar)
                .transform(new BorderedCircleTransform())
                .into(userAvatarImg);
    }

//region IView

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showMessage(int message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showErrors(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.toString());
            Log.e(LOG_TAG, "", e);
        } else {
            showMessage(getString(R.string.app_error));
        }
    }

    @Override
    public void showLoad() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, null, null);
            mProgressDialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            mProgressDialog.setContentView(new ProgressBar(this));
        }
        mProgressDialog.show();
    }

    @Override
    public void hideLoad() {
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.hide();
            }
        }
    }

//endregion


}
