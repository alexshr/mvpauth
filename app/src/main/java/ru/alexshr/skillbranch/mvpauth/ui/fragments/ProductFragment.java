package ru.alexshr.skillbranch.mvpauth.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.alexshr.skillbranch.mvpauth.R;
import ru.alexshr.skillbranch.mvpauth.data.storage.dto.ProductDTO;
import ru.alexshr.skillbranch.mvpauth.mvp.presenters.ProductPresenter;
import ru.alexshr.skillbranch.mvpauth.mvp.presenters.ProductPresenterFactory;
import ru.alexshr.skillbranch.mvpauth.mvp.views.IProductView;
import ru.alexshr.skillbranch.mvpauth.ui.activities.RootActivity;
import ru.alexshr.skillbranch.mvpauth.ui.views.AspectRatioImageView;

import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.LOG_TAG;
import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.PRODUCT_KEY;


public class ProductFragment extends Fragment implements IProductView {

    //region butterknife
    @BindView(R.id.product_name_text_view)
    TextView mProductNameTextView;
    @BindView(R.id.product_description)
    TextView mProductDescription;
    @BindView(R.id.product_image)
    AspectRatioImageView mProductImage;
    @BindView(R.id.minus_button)
    ImageButton mMinusButton;
    @BindView(R.id.product_count_text_view)
    TextView mProductCountTextView;
    @BindView(R.id.plus_button)
    ImageButton mPlusButton;
    @BindView(R.id.product_price_text_view)
    TextView mProductPriceTextView;
    @BindView(R.id.product_card)
    CardView mProductCard;

    @OnClick({R.id.minus_button, R.id.plus_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.minus_button:
                mPresenter.clickOnMinus();
                break;
            case R.id.plus_button:
                mPresenter.clickOnPlus();
        }
    }

//endregion

    private ProductPresenter mPresenter;

    public static ProductFragment newInstance(ProductDTO product) {

        Bundle args = new Bundle();
        args.putParcelable(PRODUCT_KEY, product);

        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            ProductDTO product = bundle.getParcelable(PRODUCT_KEY);
            mPresenter = ProductPresenterFactory.getInstance(product);
        }
    }

    //region lifecycle
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView();
        //mMinusButton.setOnClickListener(this);
        //mPlusButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }
//endregion

//region IProductView

    @Override
    public void showProductView(ProductDTO product) {
        mProductNameTextView.setText(product.getProductName());
        mProductDescription.setText(product.getDescription());
        mProductCountTextView.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPriceTextView.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        } else {
            mProductPriceTextView.setText(String.valueOf(product.getCount() + ".-"));
        }

        setupProductImage(product);
    }


    @Override
    public void updateProductCountView(ProductDTO product) {
        mProductCountTextView.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPriceTextView.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        }
    }

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showMessage(int message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showErrors(Throwable e) {
        getRootActivity().showErrors(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }

//endregion

    private void setupProductImage(ProductDTO product) {

        ViewTreeObserver viewTreeObserver = mProductImage.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    //находим размеры
                    mProductImage.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int imageWidth = mProductImage.getWidth();
                    int imageHeight = mProductImage.getHeight();
                    Picasso.with(getContext().getApplicationContext())
                            .load(product.getImageUrl())
                            .placeholder(R.drawable.nophoto)
                            .resize(imageWidth, imageHeight)
                            .centerInside()
                            .networkPolicy(NetworkPolicy.OFFLINE)
                            .into(mProductImage, new Callback() {
                                @Override
                                public void onSuccess() {
                                    Log.d(LOG_TAG, "product image is loaded from cache");
                                }

                                @Override
                                public void onError() {
                                    Picasso.with(getContext().getApplicationContext())
                                            .load(product.getImageUrl())
                                            .error(R.drawable.nophoto)
                                            .placeholder(R.drawable.nophoto)
                                            .resize(imageWidth, imageHeight)
                                            .centerInside()
                                            .into(mProductImage, new Callback() {
                                                @Override
                                                public void onSuccess() {
                                                    Log.d(LOG_TAG, "product image is loaded from network");
                                                }

                                                @Override
                                                public void onError() {
                                                    Log.d(LOG_TAG, "product image loading error");
                                                }
                                            });
                                }
                            });
                }
            });
        }


    }


    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }


}
