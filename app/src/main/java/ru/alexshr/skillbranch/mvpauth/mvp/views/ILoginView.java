package ru.alexshr.skillbranch.mvpauth.mvp.views;


public interface ILoginView extends IView {

    void startFbAnimation();

    void startTwAnimation();

    void startVkAnimation();

    void requestEmailFocus();

    void requestPasswordFocus();

    void setEmailError(String error);

    void setPasswordError(String error);

    String getUserEmail();

    String getUserPassword();
}
