package ru.alexshr.skillbranch.mvpauth.mvp.presenters;


import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import ru.alexshr.skillbranch.mvpauth.R;
import ru.alexshr.skillbranch.mvpauth.data.events.MessageEvent;
import ru.alexshr.skillbranch.mvpauth.data.managers.DataManager;
import ru.alexshr.skillbranch.mvpauth.mvp.models.AuthModel;
import ru.alexshr.skillbranch.mvpauth.mvp.views.ILoginView;
import ru.alexshr.skillbranch.mvpauth.utils.Utils;

import static ru.alexshr.skillbranch.mvpauth.utils.AppConfig.EMAIL_PATTERN;
import static ru.alexshr.skillbranch.mvpauth.utils.AppConfig.PASSWORD_PATTERN;
import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.LOG_TAG;
import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.MES_AUTH_FINISHED;

public class LoginPresenter extends AbstrastPresenter<ILoginView> {
    private static LoginPresenter ourInstance = new LoginPresenter();
    private static Context sAppContext = DataManager.getInstance().getAppContext();
    //идет ли загрузка
    //не храню во view, т.к. состояние может меняться при его пересоздании
    private static boolean sIsLoading;
    private AuthModel mAuthModel;

    //region eventbus
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(LOG_TAG, "onMessageEvent " + event.mes);

        switch (event.mes) {
            case MES_AUTH_FINISHED:
                onAuthFinished();
        }
    }
    //endregion

    private LoginPresenter() {
        mAuthModel = new AuthModel();
    }

    public static LoginPresenter getInstance() {
        return ourInstance;
    }

    @Override
    public void initView() {

        if (getView() != null) {
            if (sIsLoading) {
                getView().showLoad();
            } else {
                getView().hideLoad();
            }
        }
    }

    public void onViewStarted() {
        EventBus.getDefault().register(this);
    }

    public void onViewStopped() {
        EventBus.getDefault().unregister(this);
    }


    public void clickOnLogin() {
        if (getView() != null) {
            if (validateEmail() && validatePassword()) {
                String email = getView().getUserEmail();
                String password = getView().getUserPassword();
                mAuthModel.loginUser(email, password);
                sIsLoading = true;
                getView().showLoad();
            }
        }
    }

    public void onAuthFinished() {
        getView().hideLoad();
    }


    public void clickOnFb() {
        if (getView() != null) {
            getView().startFbAnimation();
        }
    }


    public void clickOnVk() {
        if (getView() != null) {
            getView().startVkAnimation();
        }
    }


    public void clickOnTwitter() {
        if (getView() != null) {
            getView().startTwAnimation();
        }
    }


    public boolean validateEmail() {
        if (getView() != null) {
            String email = getView().getUserEmail();

            if (!Utils.validate(email, EMAIL_PATTERN)) {
                getView().setEmailError(sAppContext.getString(R.string.err_msg_email));

            } else {
                getView().setEmailError(null);
                return true;
            }
        }
        return false;
    }


    public boolean validatePassword() {
        if (getView() != null) {
            String password = getView().getUserPassword();

            if (!Utils.validate(password, PASSWORD_PATTERN)) {
                getView().setPasswordError(sAppContext.getString(R.string.err_msg_password));
            } else {
                getView().setPasswordError(null);
                return true;
            }
        }
        return false;
    }
}
