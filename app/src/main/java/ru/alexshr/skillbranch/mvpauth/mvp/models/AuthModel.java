package ru.alexshr.skillbranch.mvpauth.mvp.models;

import ru.alexshr.skillbranch.mvpauth.data.managers.DataManager;
import ru.alexshr.skillbranch.mvpauth.data.managers.PreferencesManager;

public class AuthModel {
    private DataManager mDataManager;
    private PreferencesManager mPreferencesManager;

    public AuthModel() {
        mDataManager = DataManager.getInstance();
        mPreferencesManager = mDataManager.getPreferencesManager();
    }

    public boolean isAuthUser() {
        return mPreferencesManager.checkAuthToken();
    }

    public void loginUser(String email, String password) {
        mDataManager.login(email, password);
    }

}
