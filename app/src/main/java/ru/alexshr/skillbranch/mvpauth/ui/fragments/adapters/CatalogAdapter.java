package ru.alexshr.skillbranch.mvpauth.ui.fragments.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import ru.alexshr.skillbranch.mvpauth.data.storage.dto.ProductDTO;
import ru.alexshr.skillbranch.mvpauth.ui.fragments.ProductFragment;

public class CatalogAdapter extends FragmentPagerAdapter {

    private List<ProductDTO> mProductList = new ArrayList<>();

    public CatalogAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ProductFragment.newInstance(mProductList.get(position));
    }

    @Override
    public int getCount() {
        return mProductList.size();
    }

    public void addItem(ProductDTO product) {
        mProductList.add(product);
        notifyDataSetChanged();
    }
}
