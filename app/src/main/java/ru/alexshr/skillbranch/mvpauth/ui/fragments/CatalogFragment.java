package ru.alexshr.skillbranch.mvpauth.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import ru.alexshr.skillbranch.mvpauth.R;
import ru.alexshr.skillbranch.mvpauth.data.events.MessageEvent;
import ru.alexshr.skillbranch.mvpauth.data.storage.dto.ProductDTO;
import ru.alexshr.skillbranch.mvpauth.mvp.presenters.CatalogPresenter;
import ru.alexshr.skillbranch.mvpauth.mvp.views.ICatalogView;
import ru.alexshr.skillbranch.mvpauth.ui.activities.RootActivity;
import ru.alexshr.skillbranch.mvpauth.ui.fragments.adapters.CatalogAdapter;
import ru.alexshr.skillbranch.mvpauth.utils.ConstantManager;

import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.LOG_TAG;
import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.MES_AUTH_FINISHED;

public class CatalogFragment extends Fragment implements ICatalogView {

    //region butterknife
    private static final String TAG = CatalogFragment.class.getSimpleName();
    @BindView(R.id.product_view_pager)
    ViewPager mProductViewPager;
    @BindView(R.id.add_to_card_button)
    Button mAddToCardButton;
    @BindView(R.id.indicator)
    CircleIndicator mIndicator;


    @OnClick(R.id.add_to_card_button)
    public void onClick() {
        mCatalogPresenter.clickOnBuyButton(mProductViewPager.getCurrentItem());
    }
//end region

//region eventbus

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(LOG_TAG, "onMessageEvent " + event.mes);

        switch (event.mes) {
            case MES_AUTH_FINISHED:
                mCatalogPresenter.dismissLoginScreen();
        }
    }
//endregion

    private CatalogPresenter mCatalogPresenter = CatalogPresenter.getInstance();

    //region lifecycle
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        ButterKnife.bind(this, view);
        mCatalogPresenter.takeView(this);
        mCatalogPresenter.initView();
        mIndicator.setViewPager(mProductViewPager);

        return view;
    }

    @Override
    public void onDestroyView() {
        mCatalogPresenter.dropView();
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
        mCatalogPresenter.onViewStarted();
    }

    @Override
    public void onStop() {
        super.onStop();
        mCatalogPresenter.onViewStopped();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_catalog, menu);
        MenuItem item = menu.findItem(R.id.backet);
        MenuItemCompat.setActionView(item, R.layout.menu_backet_item);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        View backetView = menu.findItem(R.id.backet).getActionView();
        TextView countView = (TextView) backetView.findViewById(R.id.basket_count);
        int count = mCatalogPresenter.getBasketCount();
        countView.setText(count + "");
    }

//endregion

//region ICatalogView

    @Override
    public void showAddToCardMessage(ProductDTO productDto) {
        showMessage("Товар " + productDto.getProductName() + " успешно добавлен в корзину");
    }

    @Override
    public void showCatalogView(List<ProductDTO> productList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDTO product : productList) {
            adapter.addItem(product);
        }
        mProductViewPager.setAdapter(adapter);
    }

    @Override
    public void showLoginScreen() {
        LoginFragment fragment = null;
        if (getChildFragmentManager().findFragmentByTag(ConstantManager.TAG_LOGIN_FRAGMENT) != null) {
            fragment = (LoginFragment) getChildFragmentManager().findFragmentByTag(ConstantManager.TAG_LOGIN_FRAGMENT);
        } else {
            fragment = new LoginFragment();
        }

        fragment.show(getChildFragmentManager(), ConstantManager.TAG_LOGIN_FRAGMENT);
    }

    @Override
    public void dismissLoginScreen() {

        if (getChildFragmentManager().findFragmentByTag(ConstantManager.TAG_LOGIN_FRAGMENT) != null) {
            LoginFragment fragment = (LoginFragment) getChildFragmentManager().findFragmentByTag(ConstantManager.TAG_LOGIN_FRAGMENT);
            fragment.dismiss();
        }
    }

    @Override
    public void invalidateOptionsMenu() {
        getActivity().invalidateOptionsMenu();
    }

//endregion

//region IView

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showMessage(int message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showErrors(Throwable e) {
        getRootActivity().showErrors(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }

//endregion

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }


}
