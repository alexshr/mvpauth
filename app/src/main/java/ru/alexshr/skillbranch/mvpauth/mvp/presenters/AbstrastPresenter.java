package ru.alexshr.skillbranch.mvpauth.mvp.presenters;

import ru.alexshr.skillbranch.mvpauth.mvp.views.IView;

public abstract class AbstrastPresenter<T extends IView> {

    private T mView;

    public void takeView(T view) {
        mView = view;
    }

    public void dropView() {
        mView = null;
    }

    public abstract void initView();

    public T getView() {
        return mView;
    }
}
