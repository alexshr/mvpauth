package ru.alexshr.skillbranch.mvpauth.data.managers;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import ru.alexshr.skillbranch.mvpauth.utils.Utils;

public class MvpApplication extends Application {
    private static SharedPreferences sSharedPreferences;
    private static Context sAppContext;

    @Override
    public void onCreate() {
        super.onCreate();

        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sAppContext = getApplicationContext();
        Utils.initPicasso(sAppContext);
    }

    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }

    public static Context getAppContext() {
        return sAppContext;
    }
}
