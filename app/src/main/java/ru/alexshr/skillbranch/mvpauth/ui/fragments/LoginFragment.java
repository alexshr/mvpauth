package ru.alexshr.skillbranch.mvpauth.ui.fragments;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import ru.alexshr.skillbranch.mvpauth.R;
import ru.alexshr.skillbranch.mvpauth.mvp.presenters.LoginPresenter;
import ru.alexshr.skillbranch.mvpauth.mvp.views.ILoginView;
import ru.alexshr.skillbranch.mvpauth.ui.activities.RootActivity;

/**
 * Фрагмент для авторизации
 */
public class LoginFragment extends DialogFragment implements ILoginView {

    //region butterknife
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.progress)
    ProgressBar mProgress;
    @BindView(R.id.fb_btn)
    ImageButton mFbBtn;
    @BindView(R.id.tw_btn)
    ImageButton mTwBtn;
    @BindView(R.id.vk_btn)
    ImageButton mVkBtn;

    @BindView(R.id.login_btn)
    Button mLoginBtn;

    @BindView(R.id.login_email_et)
    EditText mEmailEt;
    @BindView(R.id.login_password_et)
    EditText mPasswordEt;

    @BindView(R.id.login_email_til)
    TextInputLayout mEmailLayout;
    @BindView(R.id.login_password_til)
    TextInputLayout mPasswordLayout;

    @OnClick({R.id.fb_btn, R.id.tw_btn, R.id.vk_btn, R.id.login_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                mPresenter.clickOnLogin();
                break;
            case R.id.fb_btn:
                mPresenter.clickOnFb();
                break;
            case R.id.tw_btn:
                mPresenter.clickOnTwitter();
                break;
            case R.id.vk_btn:
                mPresenter.clickOnVk();
                break;
        }
    }
//endregion

    private final LoginPresenter mPresenter = LoginPresenter.getInstance();
    private Unbinder mUnbinder;

//region lifecycle

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //fullscreen (временное решение)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.fragment_fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_login, container, false);
        mUnbinder = ButterKnife.bind(this, view);
        mPasswordEt.addTextChangedListener(new AuthTextWatcher(mPasswordEt));
        mEmailEt.addTextChangedListener(new AuthTextWatcher(mEmailEt));

        mPresenter.takeView(this);
        mPresenter.initView();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mUnbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.onViewStarted();
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.onViewStopped();
    }
//endregion


//region ILoginView

    @Override
    public void startFbAnimation() {
        startShakeAnimation(mFbBtn);
    }

    @Override
    public void startTwAnimation() {
        startShakeAnimation(mTwBtn);
    }

    @Override
    public void startVkAnimation() {
        startShakeAnimation(mVkBtn);
    }

    @Override
    public void requestEmailFocus() {
        requestFocus(mEmailEt);
    }

    @Override
    public void requestPasswordFocus() {
        requestFocus(mPasswordEt);
    }

    @Override
    public void setEmailError(String error) {
        if (error != null) {
            mEmailLayout.setError(error);
        } else {
            mEmailLayout.setErrorEnabled(false);
        }
    }

    @Override
    public void setPasswordError(String error) {
        if (error != null) {
            mPasswordLayout.setError(error);
        } else {
            mPasswordLayout.setErrorEnabled(false);
        }
    }

    @Override
    public String getUserEmail() {
        return mEmailEt.getText().toString().trim();
    }

    @Override
    public String getUserPassword() {
        return mPasswordEt.getText().toString().trim();
    }


    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showMessage(int message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showErrors(Throwable e) {
        getRootActivity().showErrors(e);
    }

    @Override
    public void showLoad() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoad() {
        mProgress.setVisibility(View.INVISIBLE);
    }

//endregion

    public RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    private void startShakeAnimation(View btn) {
        Animation animation = AnimationUtils.loadAnimation(getRootActivity(), R.anim.shake);
        btn.startAnimation(animation);
    }


    private void requestFocus(EditText editText) {
        if (editText.requestFocus()) {
            editText.setSelection(editText.length());
        }
    }


    static class AuthTextWatcher implements TextWatcher {

        private final EditText mEditText;
        private LoginPresenter mPresenter = LoginPresenter.getInstance();


        public AuthTextWatcher(EditText editText) {
            mEditText = editText;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (mEditText.getId()) {
                case R.id.login_email_et:
                    mPresenter.validateEmail();
                    break;
                case R.id.login_password_et:
                    mPresenter.validatePassword();
            }
        }
    }
}
