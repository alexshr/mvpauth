package ru.alexshr.skillbranch.mvpauth.mvp.presenters;

import java.util.HashMap;
import java.util.Map;

import ru.alexshr.skillbranch.mvpauth.data.storage.dto.ProductDTO;

public class ProductPresenterFactory {

    private static final Map<String, ProductPresenter> sPresenterMap = new HashMap<String, ProductPresenter>();

    private static void registerPresenter(ProductDTO productDto, ProductPresenter presenter) {
        sPresenterMap.put(String.valueOf(productDto.getId()), presenter);
    }

    public static ProductPresenter getInstance(ProductDTO productDto) {
        ProductPresenter presenter = sPresenterMap.get(String.valueOf(productDto.getId()));

        if (presenter == null) {
            presenter = ProductPresenter.newOutInstance(productDto);
            registerPresenter(productDto, presenter);
        }
        return presenter;
    }
}
