package ru.alexshr.skillbranch.mvpauth.mvp.presenters;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import ru.alexshr.skillbranch.mvpauth.R;
import ru.alexshr.skillbranch.mvpauth.data.events.MessageEvent;
import ru.alexshr.skillbranch.mvpauth.data.storage.dto.ProductDTO;
import ru.alexshr.skillbranch.mvpauth.mvp.models.CatalogModel;
import ru.alexshr.skillbranch.mvpauth.mvp.views.ICatalogView;

import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.LOG_TAG;
import static ru.alexshr.skillbranch.mvpauth.utils.ConstantManager.MES_AUTH_FINISHED;

public class CatalogPresenter extends AbstrastPresenter<ICatalogView> {

    private static CatalogPresenter ourInstance = new CatalogPresenter();

    private CatalogModel mCatalogModel;

    private List<ProductDTO> mProductList;
    private ProductDTO mProductToAdd;

    public static CatalogPresenter getInstance() {
        return ourInstance;
    }

    private CatalogPresenter() {
        mCatalogModel = new CatalogModel();
    }

    //region eventbus
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        Log.d(LOG_TAG, "onMessageEvent " + event.mes);

        switch (event.mes) {
            case MES_AUTH_FINISHED:
                getView().dismissLoginScreen();
                if (mProductToAdd != null) {
                    addCurrentProductToBasket();
                }
        }
    }
    //endregion

    @Override
    public void initView() {
        if (mProductList == null) {
            mProductList = mCatalogModel.getProductList();
        }

        if (getView() != null) {
            getView().showCatalogView(mProductList);
        }
    }


    public void clickOnBuyButton(int position) {

        if (getView() != null) {
            mProductToAdd = mProductList.get(position);
            if (checkUserAuth()) {

                addCurrentProductToBasket();

            } else {
                getView().showMessage(R.string.auth_for_buying);
                getView().showLoginScreen();
            }
        }
    }

    public void addCurrentProductToBasket() {
        mCatalogModel.addProductToBacket(mProductToAdd);
        getView().showAddToCardMessage(mProductToAdd);
        mProductToAdd = null;
        getView().invalidateOptionsMenu();
    }


    public boolean checkUserAuth() {
        return mCatalogModel.isUserAuth();
    }

    public void dismissLoginScreen() {
        getView().dismissLoginScreen();
    }

    public void onViewStarted() {
        EventBus.getDefault().register(this);
    }

    public void onViewStopped() {
        EventBus.getDefault().unregister(this);
    }

    public int getBasketCount() {
        return mCatalogModel.getBasketCount();
    }
}
